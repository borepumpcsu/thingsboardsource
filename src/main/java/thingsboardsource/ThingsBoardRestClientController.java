package thingsboardsource;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.thingsboard.rest.client.RestClient;
import org.thingsboard.server.common.data.Device;
import org.thingsboard.server.common.data.asset.Asset;
import org.thingsboard.server.common.data.id.RuleChainId;
import org.thingsboard.server.common.data.page.TextPageData;
import org.thingsboard.server.common.data.page.TextPageLink;
import org.thingsboard.server.common.data.relation.EntityRelation;
import org.thingsboard.server.common.data.rule.RuleChain;
import org.thingsboard.server.common.data.rule.RuleChainMetaData;
import org.thingsboard.server.common.data.rule.RuleNode;

import com.fasterxml.jackson.databind.JsonNode;

import ch.qos.logback.core.joran.spi.RuleStore;

public class ThingsBoardRestClientController {
	public static void main(String[] args) {
        // Default ThingsBoard REST API URL
    	String url="http://thingsboard.farmdecisiontech.net.au:9090";

    	Scanner scan = new Scanner(System.in);
    	String choice = null;
    	
        // Default Tenant Administrator credentials
    	//enter tenant username here, ie: emailaddress+tbtenant@gmail.com
        String username = "";
        //enter tenant password here
        String password = "";
        
        
        // Creating new rest client and auth with credentials
        RestClient client = new RestClient(url);
        client.login(username, password);
        
        do {
        	System.out.println("Please enter choice from options below:");
            System.out.println("1. Manage Rules Chains");
            System.out.println("2. Manage Assets");
            System.out.println("3. Manage Devices");
            System.out.println("4. Manage Dashboards");
            System.out.println("5. Exit");
            
            choice = scan.nextLine();
            switch (choice) {
            case "1":
            	System.out.print("Manage Rules Chains");
            	manageRuleChains(client);
            	break;
            case "2":
            	System.out.println("Manage Assets");
            	manageAssets(client);
            	break;
            case "3":
            	System.out.println("Manage Devices");
            	manageDevices(client);
            	break;
            case "4":
            	System.out.println("Manage Dashboards");
            	manageDashboards(client);
            	break;
            case "5":
            	System.out.println("Exiting");
            	break;
            }
        }while(!choice.equals("5")); // end of loop


        
        
        
    }

	private static void manageDashboards(RestClient client) {
		// TODO Auto-generated method stub
		
	}

	private static void manageDevices(RestClient client) {
		// TODO Auto-generated method stub
		
	}

	private static void manageAssets(RestClient client) {
		// TODO Auto-generated method stub
		
	}

	private static void manageRuleChains(RestClient client) {
		TextPageLink tplRuleChains = new TextPageLink(10);
        TextPageData<RuleChain> ruleChainsTPD = client.getRuleChains(tplRuleChains);
        List<RuleChain> ruleChainList = ruleChainsTPD.getData();
        for(RuleChain ruleChain : ruleChainList){
			/*
			 * ruleChain.getAdditionalInfo(); final JsonNode jsonNode = null;
			 * Supplier<JsonNode> arg0 = () -> jsonNode; byte[] byteArray = null;
			 * Supplier<byte[]> arg1 = () -> byteArray; JsonNode ruleChainJson =
			 * ruleChain.getJson(arg0, arg1); System.out.print(ruleChainJson.textValue());
			 */
        	Optional<RuleChainMetaData> ruleChainMetaData = client.getRuleChainMetaData(ruleChain.getId());
        	System.out.println(ruleChain.toString());
        	

        	System.out.println(ruleChainMetaData.toString());
        	Stream rcmd = ruleChainMetaData.stream();
        	System.out.println(rcmd);
        	
        }
		
	}
}